from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Firefox()
driver.get("https://udistrital-primo.hosted.exlibrisgroup.com/primo-explore/search?vid=UDISTRITAL")

while driver.title=='':
    pass

print('El titulo es: '+driver.title)
assert "Primo" in driver.title

searchBox = driver.find_element(By.ID,"searchBar")
searchBox.clear()
searchBox.send_keys("Autorregulación del aprendizaje")

searchButton = driver.find_element(By.XPATH, '//button[@aria-label="Enviar búsqueda"]')
searchButton.click()

time.sleep(5)

button50= driver.find_element(By.XPATH, '//button[text()="50"]')
button50.click()
time.sleep(5)
results=driver.find_elements(By.XPATH,"//div[contains(@id, 'SEARCH_RESULT_RECORDID')]")

for _ in results:
    print("Encontrado!!")

driver.close()