import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
import urllib.parse
import time
from exlibris_config import *

class ExlibrisWebScraper:
        
    def __init__(self):
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)

    def getQueryString(self):

        #Parse query string
        queryString=''
        for criteria in searchCriteria:
            criteria=urllib.parse.quote(criteria)
            queryString=queryString+'query=title,contains,'+criteria+',OR&'

        queryString=queryString[:len(queryString)-3]
        self.queryString=queryString
        return queryString
    
    def getSearchOptions(self):
        #Parse search options
        searchOptions=''

        for searchOption in targetOptions:
            searchOptions=searchOptions+'&'+searchOption
        self.searchOptions='&'+searchOptions
        return searchOptions
    
    def getRecords(self):        
        
        
        self.driver.get(targetURL+self.queryString+self.searchOptions)
        while self.driver.title=='':
            pass
        assert "Primo" in self.driver.title
        time.sleep(10)

        totalRecords=self.driver.find_element(By.XPATH,"//span[@class='results-count'][@role='alert']")
        totalRecords=int(totalRecords.text.strip().split()[0])

        results=[]
        f = open("resultados.html", "a")

        for page in range(round(totalRecords/10)):
            self.driver.get(targetURL+self.queryString+self.searchOptions+'&offset='+str(page*resultsPerPage))
            time.sleep(5)
            results=self.driver.find_elements(By.XPATH,"//div[contains(@id, 'SEARCH_RESULT_RECORDID')]")    
            for element in results:
                f.write(element.get_attribute('innerHTML'))
        #results=results+ driver.find_elements(By.XPATH,"//div[contains(@id, 'SEARCH_RESULT_RECORDID')]")    
        f.close()
        print(len(results))        
        return True
    
    def __del__(self):
        self.driver.close()
    
    def parseHtml(self, fileName=''):
        
        if not (fileName==''):
            self.driver.get(fileName)
        else:
            self.driver.get('file://'+ os.getcwd()+'/resultados.html')
        
        results=self.driver.find_elements(By.XPATH,"//span[@ng-if='::(!$ctrl.isEmailMode())']")
        
        dictionary=-1
        count=0
        key=0
        keys=['title', 'authors', 'journal']
        insert=True        
        records=[]
        for element in results:
            if count%3==0 and insert:
                records.append({})
                key=0
                dictionary+=1
                insert=False
            data=' '.join(element.get_attribute('innerHTML').split())
            data=data.replace('<mark>','') 
            data=data.replace('</mark>','')
            
            #If Key=0 then the string must contain some of the words defined in search
            #criteria
            
            if key==0 and any(criteria.upper() in data.upper() for criteria in searchCriteria):
                count+=1                
                records[dictionary][keys[key]]=data
                key+=1
                insert=True
            
            elif not (key==0):
                count+=1                
                records[dictionary][keys[key]]=data
                key+=1              
        
        
        myFile = open("records.txt", "w")
        for record in records:
            myFile.write(str(record) + "\n")
        
        myFile.close()
            
                
        
        
myParser=ExlibrisWebScraper()

myParser.parseHtml()
